# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/alan/Documents/fractal_markers_project/aruco-3.1.15/utils_markermap/aruco_test_markermap.cpp" "/home/alan/Documents/fractal_markers_project/aruco-3.1.15/build/utils_markermap/CMakeFiles/aruco_test_markermap.dir/aruco_test_markermap.cpp.o"
  "/home/alan/Documents/fractal_markers_project/aruco-3.1.15/utils_markermap/pcdwriter.cpp" "/home/alan/Documents/fractal_markers_project/aruco-3.1.15/build/utils_markermap/CMakeFiles/aruco_test_markermap.dir/pcdwriter.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "utils_markermap"
  "../utils_markermap"
  "../3rdparty/eigen3"
  "../src"
  "/usr/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/alan/Documents/fractal_markers_project/aruco-3.1.15/build/src/CMakeFiles/aruco.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
