# Fractal Markers Project


1. Clonar o Repositório do ArUco:

#### Abra um terminal e execute o comando de clone

Como você forneceu um link direto para a árvore do GitLab, você precisará localizar o link correto para clonar. Geralmente, o comando seria algo como:

```
git clone https://gitlab.com/alanprodam/aruco_markers_project.git
```

#### Navegue até a pasta do ArUco

Após clonar o repositório, navegue até a pasta onde está localizado o código-fonte do ArUco.

```
cd aruco_markers_project/aruco-3.1.15
```


2. Compilar a Biblioteca ArUco

#### Crie um diretório para construção dentro do diretório ArUco

Crie um diretório para os arquivos de construção:

```
mkdir build

cd build
```

#### Compile a biblioteca

Execute cmake e make para compilar a biblioteca:

```
cmake ..
make
```

#### Instalar a biblioteca (opcional). Se desejar instalar a biblioteca no seu sistema, execute:

```
sudo make install
```


3. Calibração de câmera


#### Primeiro gerar as imagens:

```
aruco_calibration live:2 aruco_calibration_board_a4.yml camera_result.yml -size 0.038 -save out_put.yalm
```

#### Segundo referenciar as imagens e rodar a calibração:

```
aruco_calibration_fromimages out_camera_calibration.yml <path>/image_calibration_1920_1080/ -size 0.038
```

#### Dicas em relação ao resultado RMS:

Em calibração de câmeras, um RMS abaixo de 1 é frequentemente considerado muito bom, enquanto valores acima de 1 podem ser aceitáveis, dependendo da precisão exigida pela sua aplicação específica. O erro de reprojeção (repj error) de 1.98789 também fornece uma indicação de quão precisas são as projeções dos pontos 3D para os pontos 2D na imagem. Assim como o RMS, valores mais baixos são melhores.
